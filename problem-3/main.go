package main

func main() {
	number := 600851475143
	for i := 2; i <= number/2; i++ {
		if number%i == 0 {
			toCheck := number / i
			if primeCheck(toCheck) {
				println(toCheck)
				break
			}
		}
	}
}

func primeCheck(number int) bool {
	for i := 2; i <= number/2; i++ {
		if number%i == 0 {
			return false
		}
	}
	return true
}
