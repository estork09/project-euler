package main

//

func main() {
	var sum int
	for i := 1; i < 10; i++ {
		if i%3 == 0 || i%5 == 0 {
			sum = sum + i
		}
	}
	print(sum)

}
