package main

func main() {
	var number int
	max := 0
	for i := 999; i > 99; i-- {
		for j := 999; j > 99; j-- {
			number = i * j
			if number == reverseInt(number) {
				if number > max {
					max = number
				}
			}
		}
	}
	println(max)

}

func reverseInt(i int) int {
	flipped := 0
	for i > 0 {
		remainder := i % 10
		flipped *= 10
		flipped += remainder
		i /= 10
	}
	return flipped
}
